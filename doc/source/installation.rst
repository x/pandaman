============
Installation
============

At the command line::

    $ pip install pandaman

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv pandaman
    $ pip install pandaman
